//------------Ejercicio 1-----------------------
$(document).on("click", "#boton1", function () {
    $("#div1").append("<h1>Mario</h1>");
});

//------------Ejercicio 2-----------------------
$(document).on("click", "#boton2", function () {
    var lista = ["Hola", "¿Que tal?", "Yo bien, ¿y tu?", "Muy bien.chao."]
    $(lista).each(function (i, item) {
        $("#lista").append("<li>" + item + "</li>");
    });
});

var indice = 0;
$(document).on("click", "#boton2b", function () {
    var lista = ["Hola", "¿Que tal?", "Yo bien, ¿y tu?", "Muy bien.chao."]
    $("#lista").append("<li>" + lista[indice] + "</li>");
    indice++;
});

//------------------Ejercicio 3----------------------


$(document).on("click", "#boton3", function () {
    var listaTelefonos = [{ nombre: "ana", tel: "android" }, { nombre: "javi", tel: "iphone" }, { nombre: "carmen", tel: "android" }, { nombre: "rubén", tel: "android" },];
    $(listaTelefonos).each(function (i, el) {
        $("#cuerpotabla").append("<tr><td>" + el.nombre + "</td><td>" + el.tel + "</td></tr>");
    });
});

//Peticion Ajax--------------------------
$(document).on("click", "#botoncliente", function () {
    $.getJSON("clientes.json", function (data) {
        data.clientes.forEach(function (cliente) {
            $("#cuerpotabla2").append("<tr><td>"+cliente.id+"</td><td>"+cliente.nombre+"</td><td>"+cliente.tel+"</td><td>"+cliente.email+"</td></tr>")
        });
    });
})